from selenium import webdriver
from bs4 import BeautifulSoup as BS
import sqlite3
import time
driver = webdriver.Chrome('C:/chromedriver.exe')

conn = sqlite3.connect("kof.sqlite3")
cursor = conn.cursor()

links = {
    'https://melbet.ru/line/football/':'Футбол',
    'https://melbet.ru/line/ice-hockey/':'Хоккей',
    'https://melbet.ru/line/rugby/':'Регби',
    'https://melbet.ru/line/basketball/':'Баскетбол'
}

for link in links:
    print(link)
    driver.get(link)
    time.sleep(2)
    a = driver.page_source
    auth_bs = BS(a, 'html.parser')
    a3=auth_bs.find_all("div", class_="kofsTableLineNums")
    for j in range(len(a3)):
        print("---")
        date1=a3[j].find("span", class_="date")
        date2 = date1.text
        date3 = date2[date2.find('.')+1:date2.rfind('.')]+date2[:date2.find('.')]
        time1=a3[j].find("span", class_="time")
        time2 = time1.text
        time3 = time2[:time2.find(':')] + time2[time2.find(':')+1:]
        aa=a3[j].find_all("span", class_="team")
        itog=a3[j].find_all("span", class_="num")
        print(links[link])
        print(date3)
        print(time3)
        print(aa[0].text+'- '+aa[1].text)
        if itog[0].text == '-':
            i1 = 0
        else:
            i1 = str(itog[0].text)
        if itog[1].text == '-':
            i2 = 0
        else:
            i2 = str(itog[1].text)
        if itog[2].text == '-':
            i3 = 0
        else:
            i3 = str(itog[2].text)
        print(i1)
        print(i2)
        print(i3)
        info = [('melbet.ru',links[link],date3,time3,aa[0].text+' - '+aa[1].text,i1,i2,i3)]
        cursor.executemany("INSERT INTO kof VALUES (?,?,?,?,?,?,?,?)", info)
        conn.commit()
conn.close()
driver.quit()

"""
<div class="kofsTableBody"><div class="kofsTableLine"><div class="kofsTableLineNums "><div class="name clear none"><div class="arr fl" title="Больше информации" id="more_245714412"></div><a class="nameLink fl clear" id="245714412" liga="110163" href="line/football/110163-italy-serie-a/79255041-cagliari-calcio-sassuolo-calcio/"><span class="teams fl"><span class="team">Кальяри </span><span class="team">Сассуоло </span></span><span class="dateCon fl"><span class="time">20:30</span><span class="date">18.07.2020</span></span></a><div class="dopColCon fl" id="dop_245714412"><div class="dopNum">(<b>472.</b>)</div><div class="num" id="num_245714412"> +1607</div></div><div class="team team__dop" title=""></div></div><div class="kofs clear"><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="1" data-eventid="24571441201" data-gameid="245714412" data-type="1" data-block="false" data-coef="3.76" data-dir="3">3.76</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="1" data-eventid="24571441202" data-gameid="245714412" data-type="2" data-block="false" data-coef="4" data-dir="3">4</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="1" data-eventid="24571441203" data-gameid="245714412" data-type="3" data-block="false" data-coef="2" data-dir="3">2</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="8" data-eventid="24571441204" data-gameid="245714412" data-type="4" data-block="false" data-coef="1.9" data-dir="3">1.9</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="8" data-eventid="24571441205" data-gameid="245714412" data-type="5" data-block="false" data-coef="1.29" data-dir="3">1.29</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="8" data-eventid="24571441206" data-gameid="245714412" data-type="6" data-block="false" data-coef="1.315" data-dir="3">1.315</span></span></span><span class="kof fl l2"><span class="mid"><span class="txt">(1.5)</span><span class="num " data-playerid="0" data-param="1.5" data-groupid="2" data-eventid="2457144121.57" data-gameid="245714412" data-type="7" data-block="false" data-coef="1.32" data-dir="3">1.32</span></span></span><span class="kof fl l2"><span class="mid"><span class="txt">(-1.5)</span><span class="num " data-playerid="0" data-param="-1.5" data-groupid="2" data-eventid="245714412-1.58" data-gameid="245714412" data-type="8" data-block="false" data-coef="3.26" data-dir="3">3.26</span></span></span><span class="kof fl l2"><span class="mid"><span class="txt">(3.5)</span><span class="num " data-playerid="0" data-param="3.5" data-groupid="17" data-eventid="2457144123.59" data-gameid="245714412" data-type="9" data-block="false" data-coef="2.31" data-dir="3">2.31</span></span></span><span class="kof fl l2"><span class="mid"><span class="txt">(3.5)</span><span class="num " data-playerid="0" data-param="3.5" data-groupid="17" data-eventid="2457144123.510" data-gameid="245714412" data-type="10" data-block="false" data-coef="1.715" data-dir="3">1.715</span></span></span></div></div><div class="kofsTableLineDop" id="kofsTableLineDop_245714412"></div></div></div>
<div class="kofsTableLine"><div class="kofsTableLineNums "><div class="name clear none"><div class="arr fl" title="Больше информации" id="more_245714412"></div><a class="nameLink fl clear" id="245714412" liga="110163" href="line/football/110163-italy-serie-a/79255041-cagliari-calcio-sassuolo-calcio/"><span class="teams fl"><span class="team">Кальяри </span><span class="team">Сассуоло </span></span><span class="dateCon fl"><span class="time">20:30</span><span class="date">18.07.2020</span></span></a><div class="dopColCon fl" id="dop_245714412"><div class="dopNum">(<b>472.</b>)</div><div class="num" id="num_245714412"> +1607</div></div><div class="team team__dop" title=""></div></div><div class="kofs clear"><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="1" data-eventid="24571441201" data-gameid="245714412" data-type="1" data-block="false" data-coef="3.76" data-dir="3">3.76</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="1" data-eventid="24571441202" data-gameid="245714412" data-type="2" data-block="false" data-coef="4" data-dir="3">4</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="1" data-eventid="24571441203" data-gameid="245714412" data-type="3" data-block="false" data-coef="2" data-dir="3">2</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="8" data-eventid="24571441204" data-gameid="245714412" data-type="4" data-block="false" data-coef="1.9" data-dir="3">1.9</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="8" data-eventid="24571441205" data-gameid="245714412" data-type="5" data-block="false" data-coef="1.29" data-dir="3">1.29</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="8" data-eventid="24571441206" data-gameid="245714412" data-type="6" data-block="false" data-coef="1.315" data-dir="3">1.315</span></span></span><span class="kof fl l2"><span class="mid"><span class="txt">(1.5)</span><span class="num " data-playerid="0" data-param="1.5" data-groupid="2" data-eventid="2457144121.57" data-gameid="245714412" data-type="7" data-block="false" data-coef="1.32" data-dir="3">1.32</span></span></span><span class="kof fl l2"><span class="mid"><span class="txt">(-1.5)</span><span class="num " data-playerid="0" data-param="-1.5" data-groupid="2" data-eventid="245714412-1.58" data-gameid="245714412" data-type="8" data-block="false" data-coef="3.26" data-dir="3">3.26</span></span></span><span class="kof fl l2"><span class="mid"><span class="txt">(3.5)</span><span class="num " data-playerid="0" data-param="3.5" data-groupid="17" data-eventid="2457144123.59" data-gameid="245714412" data-type="9" data-block="false" data-coef="2.31" data-dir="3">2.31</span></span></span><span class="kof fl l2"><span class="mid"><span class="txt">(3.5)</span><span class="num " data-playerid="0" data-param="3.5" data-groupid="17" data-eventid="2457144123.510" data-gameid="245714412" data-type="10" data-block="false" data-coef="1.715" data-dir="3">1.715</span></span></span></div></div><div class="kofsTableLineDop" id="kofsTableLineDop_245714412"></div></div>
<div class="kofsTableLineNums "><div class="name clear none"><div class="arr fl" title="Больше информации" id="more_245714412"></div><a class="nameLink fl clear" id="245714412" liga="110163" href="line/football/110163-italy-serie-a/79255041-cagliari-calcio-sassuolo-calcio/"><span class="teams fl"><span class="team">Кальяри </span><span class="team">Сассуоло </span></span><span class="dateCon fl"><span class="time">20:30</span><span class="date">18.07.2020</span></span></a><div class="dopColCon fl" id="dop_245714412"><div class="dopNum">(<b>472.</b>)</div><div class="num" id="num_245714412"> +1607</div></div><div class="team team__dop" title=""></div></div><div class="kofs clear"><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="1" data-eventid="24571441201" data-gameid="245714412" data-type="1" data-block="false" data-coef="3.76" data-dir="3">3.76</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="1" data-eventid="24571441202" data-gameid="245714412" data-type="2" data-block="false" data-coef="4" data-dir="3">4</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="1" data-eventid="24571441203" data-gameid="245714412" data-type="3" data-block="false" data-coef="2" data-dir="3">2</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="8" data-eventid="24571441204" data-gameid="245714412" data-type="4" data-block="false" data-coef="1.9" data-dir="3">1.9</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="8" data-eventid="24571441205" data-gameid="245714412" data-type="5" data-block="false" data-coef="1.29" data-dir="3">1.29</span></span></span><span class="kof fl l1"><span class="mid"><span class="num " data-playerid="0" data-param="0" data-groupid="8" data-eventid="24571441206" data-gameid="245714412" data-type="6" data-block="false" data-coef="1.315" data-dir="3">1.315</span></span></span><span class="kof fl l2"><span class="mid"><span class="txt">(1.5)</span><span class="num " data-playerid="0" data-param="1.5" data-groupid="2" data-eventid="2457144121.57" data-gameid="245714412" data-type="7" data-block="false" data-coef="1.32" data-dir="3">1.32</span></span></span><span class="kof fl l2"><span class="mid"><span class="txt">(-1.5)</span><span class="num " data-playerid="0" data-param="-1.5" data-groupid="2" data-eventid="245714412-1.58" data-gameid="245714412" data-type="8" data-block="false" data-coef="3.26" data-dir="3">3.26</span></span></span><span class="kof fl l2"><span class="mid"><span class="txt">(3.5)</span><span class="num " data-playerid="0" data-param="3.5" data-groupid="17" data-eventid="2457144123.59" data-gameid="245714412" data-type="9" data-block="false" data-coef="2.31" data-dir="3">2.31</span></span></span><span class="kof fl l2"><span class="mid"><span class="txt">(3.5)</span><span class="num " data-playerid="0" data-param="3.5" data-groupid="17" data-eventid="2457144123.510" data-gameid="245714412" data-type="10" data-block="false" data-coef="1.715" data-dir="3">1.715</span></span></span></div></div>
<div class="name clear none"><div class="arr fl" title="Больше информации" id="more_245714412"></div><a class="nameLink fl clear" id="245714412" liga="110163" href="line/football/110163-italy-serie-a/79255041-cagliari-calcio-sassuolo-calcio/"><span class="teams fl"><span class="team">Кальяри </span><span class="team">Сассуоло </span></span><span class="dateCon fl"><span class="time">20:30</span><span class="date">18.07.2020</span></span></a><div class="dopColCon fl" id="dop_245714412"><div class="dopNum">(<b>472.</b>)</div><div class="num" id="num_245714412"> +1607</div></div><div class="team team__dop" title=""></div></div>
<a class="nameLink fl clear" id="246098800" liga="88637" href="line/football/88637-england-premier-league/79367788-bournemouth-southampton/"><span class="teams fl"><span class="team">Борнмут </span><span class="team">Саутгемптон </span></span><span class="dateCon fl"><span class="time">16:00</span><span class="date">19.07.2020</span></span></a>
<span class="teams fl"><span class="team">Борнмут </span><span class="team">Саутгемптон </span></span>
<span class="team">Борнмут </span>

"""
